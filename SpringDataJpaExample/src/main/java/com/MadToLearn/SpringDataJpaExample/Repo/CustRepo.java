package com.MadToLearn.SpringDataJpaExample.Repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.MadToLearn.SpringDataJpaExample.Model.Customer;

@Repository
public interface CustRepo extends CrudRepository<Customer,Integer>{

}
