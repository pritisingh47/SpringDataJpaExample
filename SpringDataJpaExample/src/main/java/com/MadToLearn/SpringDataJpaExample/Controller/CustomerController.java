package com.MadToLearn.SpringDataJpaExample.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.MadToLearn.SpringDataJpaExample.Model.Customer;
import com.MadToLearn.SpringDataJpaExample.Repo.CustRepo;

@RestController
public class CustomerController {
	
	@Autowired
	CustRepo custrepo;
	
	@RequestMapping("/getCustDetails")
	public List<Customer> getCustDetails()
	{
		List<Customer> cust_list=new ArrayList<>();
		custrepo.findAll().forEach(cust_list::add);
		return cust_list;
	}
}
