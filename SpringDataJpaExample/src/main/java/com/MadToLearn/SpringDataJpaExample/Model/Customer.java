package com.MadToLearn.SpringDataJpaExample.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Customer {
@Id
int Id;
String Name;
int Age;
String Address;
int Salary;

public Customer()
{
	
}
public Customer(int id, String name, int age, String address, int salary) {
	super();
	Id = id;
	Name = name;
	Age = age;
	Address = address;
	Salary = salary;
}
public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public int getAge() {
	return Age;
}
public void setAge(int age) {
	Age = age;
}
public String getAddress() {
	return Address;
}
public void setAddress(String address) {
	Address = address;
}
public int getSalary() {
	return Salary;
}
public void setSalary(int salary) {
	Salary = salary;
}
}
