Spring Boot, H2,Spring Data JPA Rest API Tutorial
Build Restful CRUD API for a simple CRUD application using Spring Boot, H2, Spring Data JPA.

Requirements
Spring Tool Suite

Steps to Setup
1. Clone the application

git clone https://github.com/pritisingh47/SpringDataJpaExample.git

2. Change H2 username and password as per your installation

open src/main/resources/application.properties

change spring.datasource.username and spring.datasource.password as per your H2 installation

3. Build and run the app using maven

mvn package
java -jar target/CRUD-Ex-1.0.0.jar
Alternatively, you can run the app without packaging it using -

mvn spring-boot:run
The app will start running at http://localhost:1000.

Explore Rest APIs
The app defines following CRUD APIs.

GET /getCustDetails

POST /addCustDtls

GET /getCustById/{id

PATCH /updateCustDtl/{id}

DELETE /deleteCustomerById/{id}
You can test them using postman or any other rest client.
